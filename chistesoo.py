#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Simple program to parse a chistes XML file"""
"""Ejercicio 5"""

from xml.dom.minidom import parse


def main(path):
    ob1 = Humor(path)
    chistes = ob1.jokes()
    for chiste in chistes:
        print(f"{(chiste[1])}\n")
        print(f"{(chiste[3])}\n")
        print(f"{chiste[5]}\n.")



class Humor:
    def __init__(self, path):
        self.path = path

    def jokes(self):
        """Programa principal"""
        document = parse(self.path)
        if document.getElementsByTagName('humor'):
            jokes = document.getElementsByTagName('chiste')
            ChistesMalos = []
            ChistesMalisimos = []
            ChistesNormales = []
            ChistesBuenisimos = []
            ChistesBuenos = []

            for joke in jokes:
                score = joke.getAttribute('calificacion')
                questions = joke.getElementsByTagName('pregunta')
                question = questions[0].firstChild.nodeValue.strip()
                answers = joke.getElementsByTagName('respuesta')
                answer = answers[0].firstChild.nodeValue.strip()
                Dic = {"Score": score, "Question": question, "Answer": answer}
                if score == "buenisimo":
                    ChistesBuenisimos.append(joke)
                    ChistesBuenisimos.append(Dic)
                if score == "bueno":
                    ChistesBuenos.append(joke)
                    ChistesBuenos.append(Dic)
                if score == "regular":
                    ChistesNormales.append(joke)
                    ChistesNormales.append(Dic)
                if score == "malo":
                    ChistesMalos.append(joke)
                    ChistesMalos.append(Dic)
                if score == "malisimo":
                    ChistesMalisimos.append(joke)
                    ChistesMalisimos.append(Dic)

            Lista = [ChistesBuenisimos + ChistesBuenos + ChistesNormales + ChistesMalos + ChistesMalisimos]
            return Lista

        else:
            print("Root Element is not humor")


if __name__ == "__main__":
  main("chistes.xml")
