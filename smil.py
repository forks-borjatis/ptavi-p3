#!/usr/bin/python3
# -*- coding: utf-8 -*-

import xml.dom.minidom
import xml.etree.ElementTree


class Element:

    def __init__(self, name, attrs):
        """Esto es el método iniciliazador"""
        self.elemento = name
        self.dict = attrs

    def name(self):
        return self.elemento

    def attrs(self):
        return self.dict


class SMIL:
    def __init__(self, path):
        self.path = path

    def elements(self):
        document = xml.etree.ElementTree.parse(self.path)
        e = list()
        tipos = list()

        for element in document.iter():
            e.append(element)

        for elemento in e:
            tipos.append(Element(elemento.tag, elemento.attrib))

        return tipos




if __name__ == '__main__':
    ob1 = SMIL('karaoke.smil')
    ob1.elements()
